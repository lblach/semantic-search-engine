#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <filesystem>
#include <fstream>


#include <opencv2/core.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;
using namespace dnn;


namespace fs = std::filesystem;

namespace engine {

	class SearchEngine
	{
	public:		
        SearchEngine(float confThreshold, float nmsThreshold, int inpWidth, int inpHeight)
			:	mConfThreshold(confThreshold),
				mNmsThreshold(nmsThreshold),
				mInpWidth(inpWidth),
				mInpHeight(inpHeight)								
		{		                       
		};	

		void loadFolderToBeIndexed(int argc, char** argv, std::string path) { 
			//load all files (paths to files) from a folder
            const fs::path pathToTestPictures(path);
			const fs::path pathToFolder{ argc >= 2 ? argv[1] : pathToTestPictures };
			//store all files
			for (const auto& entry : fs::directory_iterator(pathToFolder)) {
				const auto fileName = entry.path().string();
				mFileNames.push_back(fileName);
			}
		};	

		void loadObjectClassNames(std::string pathToObjectClassNamesFileAsStr) {
            fs::path pathToObjectClassNamesFile(pathToObjectClassNamesFileAsStr);                        
			ifstream ifs(pathToObjectClassNamesFile.c_str());
			string line;
			while (getline(ifs, line)) mObjectClassNames.push_back(line);
		}

		void setObjectClassNamesToBeDetected(std::vector<string> hasObjectNames, std::vector<string> noObjectNames) {
            for (auto name : hasObjectNames) {
                mHasObjects.push_back(getObjectId(name));                
            }
            for (auto name : noObjectNames) {
                mNoObjects.push_back(getObjectId(name));                
            }
        }

		void loadDNNetwork(String pathToModelConfigFileAsStr, String pathToModelWeightsFileAsStr) {            
            fs::path pathToModelConfigFile(pathToModelConfigFileAsStr);                        
            fs::path pathToModelWeightsFile(pathToModelWeightsFileAsStr);            
			mNet = readNetFromDarknet(pathToModelConfigFile.make_preferred().string(), pathToModelWeightsFile.make_preferred().string());
			mNet.setPreferableBackend(DNN_BACKEND_OPENCV);
			mNet.setPreferableTarget(DNN_TARGET_CPU);
		}

		void build() {			
            std::cout << "Build Search Engine Index";
			for (const auto& fileName : mFileNames) {                                
                std::cout << ".";
                auto objectsDetected = processAndShowFrame(fileName);  
                for (const auto& object : objectsDetected) {                   
                    mIndex.insert(std::pair<std::string, std::pair<int, int>>(fileName, std::pair(object.first, object.second)));
                }
			}
            std::cout << "Done" << std::endl;
		}

		std::vector<std::string> search() {  
            std::cout << "Search objects... ";
            
            std::sort(mHasObjects.begin(), mHasObjects.end());
            std::sort(mNoObjects.begin(), mNoObjects.end());

            for (const auto& fileName : mFileNames) {
                typedef std::multimap<std::string, std::pair<int, int>>::iterator INDEXEngineIterator;
                // It returns a pair representing the range of elements with key equal to a file name (string)
                std::pair<INDEXEngineIterator, INDEXEngineIterator> result = mIndex.equal_range(fileName);
                // Iterate over the range
                std::vector<int> foundObjects;
                foundObjects.clear();
                
                for (INDEXEngineIterator it = result.first; it != result.second; it++) {                    
                    foundObjects.push_back(it->second.first);
                }

                std::sort(foundObjects.begin(), foundObjects.end());

                std::vector<int> intersectionVector;
                std::set_intersection(foundObjects.begin(), foundObjects.end(), mNoObjects.begin(), mNoObjects.end(), std::back_inserter(intersectionVector));

                if (std::includes(foundObjects.begin(), foundObjects.end(), mHasObjects.begin(), mHasObjects.end()) &&
                    intersectionVector.size() == 0) {                    
                    mDetectedFileNames.push_back(fileName);
                }
            }           
            std::cout << "Done" << std::endl;
            return mDetectedFileNames;
        }

        void show() {
            std::cout << "Show files with the detected objects... " << std::endl;
            
            std::vector<std::pair<std::string, int>> filesToShow;            

            // collect file names to be presented (sorted according to the area of ​​objects found in the files/pictures)
            for (auto fileName : mDetectedFileNames) {   
                auto totalArea{ 0 };
                // read the area of ​​objects found in the file from the engine index
                for (const auto& element : mIndex) {
                    if (element.first == fileName && (std::find(mHasObjects.begin(), mHasObjects.end(), element.second.first) != std::end(mHasObjects))) {                        
                        totalArea += element.second.second;
                    }                    
                }   
                filesToShow.push_back(std::make_pair(fileName, totalArea));
            }

            //  sort the files before displaying
            sort(filesToShow.begin(), filesToShow.end(),
                [](const pair<std::string, int>& lhs, const pair<std::string, int>& rhs) {
                    return lhs.second > rhs.second; });

            std::cout << std::endl;
            std::cout <<"\tList of files/pictures to be presented (up to 3 pictures):" << std::endl;
            auto count{ 0 };
            for (auto file : filesToShow) { 
                if (count < 3) {
                    std::cout << "\tfile name: " << file.first << " => total area of detected objects : " << file.second << std::endl;
                    processAndShowFrame(file.first, true);
                    count++;
                }
                else {
                    break;
                }
            }
            
            std::cout << "Done" << std::endl;
            std::cout << "Press a key to quit..."; 
            waitKey(0);
            std::cout << "Done" << std::endl;
        }


		~SearchEngine() {};

	private:			
		float mConfThreshold;
		float mNmsThreshold;
		int mInpWidth;
		int mInpHeight;
		std::vector<std::string> mFileNames;
		vector<string> mObjectClassNames;
		std::vector<int> mHasObjects;
        std::vector<int> mNoObjects;
		Net mNet;
		// Engine Index structure
		std::multimap<std::string, std::pair<int, int>> mIndex;        
        std::vector<std::string> mDetectedFileNames;

		int getObjectId(std::string classObjectName) {
            for (auto i = 0; i < mObjectClassNames.size(); i++) {
                if (classObjectName == mObjectClassNames[i]) {
                    return i;
                }
            }
            return -1;
        }

		std::vector<std::pair<int, int>> processAndShowFrame(std::string fileName, bool toShowFrame = false) {
            Mat input, blob;
            
            input = imread(fileName);

            // Create a 4D blob 
            blobFromImage(input, blob, 1 / 255.0, Size(mInpWidth, mInpHeight), Scalar(0, 0, 0), true, false);

            //Sets the input to the network
            mNet.setInput(blob);

            // Runs the forward pass to get output of the output layers
            vector<Mat> outs;
            mNet.forward(outs, getOutputsNames(mNet));

            std::vector<std::pair<int, int>> objectsDetected{};
            if (toShowFrame) {
                // Remove the bounding boxes with low confidence and get detected objects for a picture
                objectsDetected = postprocess(input, outs, true);                
                imshow(fileName, input);
            }
            else {
                // Remove the bounding boxes with low confidence and get detected objects for a picture
                objectsDetected = postprocess(input, outs, false);
            }

            return objectsDetected;
        }

        // Draw the predicted bounding box
        void drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame)
        {
            //Draw a rectangle displaying the bounding box
            rectangle(frame, Point(left, top), Point(right, bottom), Scalar(255, 255, 255), 1);

            //Get the label for the class name and its confidence
            string conf_label = format("%.2f", conf);
            string label = "";
            if (!mObjectClassNames.empty())
            {
                label = mObjectClassNames[classId] + ":" + conf_label;
            }

            //Display the label at the top of the bounding box
            int baseLine;
            Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
            top = max(top, labelSize.height);
            rectangle(frame, Point(left, top - labelSize.height), Point(left + labelSize.width, top + baseLine), Scalar(255, 255, 255), FILLED);
            putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 0), 1, LINE_AA);
        }

        // Remove the bounding boxes with low confidence using non-maxima suppression
        std::vector<std::pair<int, int>> postprocess(Mat& frame, const vector<Mat>& outs, bool onlySelectedObjects = false)
        {
            vector<int> classIds;
            vector<float> confidences;
            vector<Rect> boxes;


            for (size_t i = 0; i < outs.size(); ++i)
            {
                // Scan through all the bounding boxes output from the network and keep only the
                // ones with high confidence scores. Assign the box's class label as the class
                // with the highest score for the box.
                float* data = (float*)outs[i].data;
                for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
                {
                    Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
                    Point classIdPoint;
                    double confidence;
                    // Get the value and location of the maximum score
                    minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
                    if (confidence > mConfThreshold)
                    {
                        int centerX = (int)(data[0] * frame.cols);
                        int centerY = (int)(data[1] * frame.rows);
                        int width = (int)(data[2] * frame.cols);
                        int height = (int)(data[3] * frame.rows);
                        int left = centerX - width / 2;
                        int top = centerY - height / 2;

                        classIds.push_back(classIdPoint.x);
                        confidences.push_back((float)confidence);
                        boxes.push_back(Rect(left, top, width, height));
                    }
                }
            }

            // Perform non maximum suppression to eliminate redundant overlapping boxes with
            // lower confidences
            vector<int> indices;
            NMSBoxes(boxes, confidences, mConfThreshold, mNmsThreshold, indices);

            std::vector<std::pair<int, int>> objectsDetected;
            for (size_t i = 0; i < indices.size(); ++i)
            {
                int idx = indices[i];                
                if (!onlySelectedObjects) {
                    Rect box = boxes[idx];
                    drawPred(classIds[idx], confidences[idx], box.x, box.y,
                        box.x + box.width, box.y + box.height, frame);
                    objectsDetected.push_back(std::make_pair(classIds[idx], box.width * box.height));                    
                }
                else {
                    // draw box only for selected objects
                    if (std::find(mHasObjects.begin(), mHasObjects.end(), classIds[idx]) != std::end(mHasObjects)){                        
                        Rect box = boxes[idx];
                        drawPred(classIds[idx], confidences[idx], box.x, box.y,
                            box.x + box.width, box.y + box.height, frame);
                        objectsDetected.push_back(std::make_pair(classIds[idx], box.width * box.height));
                    }
                }               
            }

            return objectsDetected;
        }

        // Get the names of the output layers
        vector<String> getOutputsNames(const Net& net)
        {
            static vector<String> names;
            if (names.empty())
            {
                //Get the indices of the output layers, i.e. the layers with unconnected outputs
                vector<int> outLayers = net.getUnconnectedOutLayers();                
                //get the names of all the layers in the network
                vector<String> layersNames = net.getLayerNames();                
                // Get the names of the output layers in names
                names.resize(outLayers.size());
                for (size_t i = 0; i < outLayers.size(); ++i) {
                    names[i] = layersNames[outLayers[i] - 1];
                }
            }            
            return names;
        }

	};	
		 

}