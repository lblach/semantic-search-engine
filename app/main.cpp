#include <engine/engine.hpp>
#include "config.h"

int main(int argc, char** argv) {       

    const std::vector<string> hasObjects{ "person"};
    const std::vector<string> noObjects{};

    // initialize Search Engine
    engine::SearchEngine searchEngine(0.5f, 0.4f, 640, 480);

	searchEngine.loadFolderToBeIndexed(argc, argv, TEST_PICTURES_PATH);

	searchEngine.loadObjectClassNames(COCO_NAMES_PATH);


	searchEngine.setObjectClassNamesToBeDetected(hasObjects, noObjects);

	searchEngine.loadObjectClassNames(COCO_NAMES_PATH);

    searchEngine.setObjectClassNamesToBeDetected(hasObjects, noObjects);

    searchEngine.loadDNNetwork(YOLOV3_CFG_PATH, YOLOV3_WEIGHTS_PATH);

	searchEngine.build();

	searchEngine.search();   
	
    searchEngine.show();
              
    return 0;
}