# Semantic Search Engine Project

## Installation on Windows
Assumption:
- Microsoft Visual Studio Community 2019 Version 16.7.1 [https://visualstudio.microsoft.com/pl/thank-you-downloading-visual-studio/?sku=community&rel=16]
- vcpkg manager is installed as a source of libraries [https://github.com/microsoft/vcpkg]
- OpenCV (>4.1) is installed and integrated with Windows
- CMake (>3.14) is installed [https://cmake.org/download/]


### Install and integrate opencv library using vcpkg
```sh
vcpkg install opencv4:x64-windows
vcpkg integrate install
```
###  Goto the project folder and build the project with CMake
```sh
semantic-search-engine\build>cmake ..
semantic-search-engine\build>cmake --build . --config Release
```
### Run
```sh
	semantic-search-engine\build>./app/Release/engineapp.exe
	                            or
	semantic-search-engine\build>./app/Release/engineapp.exe <path-to-picture-folder>
```	
